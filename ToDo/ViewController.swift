//
//  ViewController.swift
//  ToDo
//
//  Created by Nadishan on 5/18/19.
//  Copyright © 2019 iit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var dateTextField: UITextField!
   
    private var datePicker: UIDatePicker?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        dateTextField.inputView = datePicker
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    


}

