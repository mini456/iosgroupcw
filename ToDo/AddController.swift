//
//  AddController.swift
//  ToDo
//
//  Created by Minal Abayasekara on 5/20/19.
//  Copyright © 2019 iit. All rights reserved.
//

import Foundation
import UIKit

class AddController: UIViewController {
    
    
    @IBOutlet weak var dateTextField: UITextField!
    
    @IBOutlet weak var timeTextField: UITextField!
    private var datePicker: UIDatePicker?
    private var timePicker: UIDatePicker?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        

        timePicker = UIDatePicker()
        timePicker?.datePickerMode = .time
        timePicker?.addTarget(self, action: #selector(timeChanged(timePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
        dateTextField.inputView = datePicker
        timeTextField.inputView = timePicker
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func dateChanged(datePicker: UIDatePicker){
       let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @objc func timeChanged(timePicker: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:MM"
        timeTextField.text = dateFormatter.string(from: timePicker.date)
        view.endEditing(true)
    }
    
    @objc func viewTapped(gestureRecognizer:UITapGestureRecognizer){
        view.endEditing(true)
    }
    
}
